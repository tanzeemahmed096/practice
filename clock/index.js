const hours = document.querySelector(".hour")
const minutes = document.querySelector(".minutes")
const seconds = document.querySelector(".seconds")
setInterval(setTime, 1000);

function setTime() {
    const date = new Date();
    const second = date.getSeconds();
    const minute = date.getMinutes();
    const hour = date.getHours();

    const hourDegree = (((hour - 12) / 12) * 360) + 90;
    const minutesDegree = ((minute / 60) * 360) + 90;
    const secondsDegree = ((second / 60) * 360) + 90;
    seconds.style.transform = `rotate(${secondsDegree}deg)`
    minutes.style.transform = `rotate(${minutesDegree}deg)`
    hours.style.transform = `rotate(${hourDegree}deg)`
}